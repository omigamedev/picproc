#pragma once

#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#define GLM_FORCE_RADIANS
#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL
#define GLEW_STATIC
#define IS60_USING_DLL

#include "targetver.h"

#include <windows.h>
#include <ppl.h>

#include <stdio.h>
#include <tchar.h>

#include <map>
#include <deque>
#include <mutex>
#include <atomic>
#include <chrono>
#include <thread>
#include <vector>
#include <exception>
#include <algorithm>
#include <functional>
#include <condition_variable>

#include <GL/glew.h>
#include <GL/wglew.h>
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/compatibility.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include <stb.h>
#include <stb_image.h>
#include <stb_image_write.h>

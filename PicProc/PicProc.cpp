#include "stdafx.h"

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb.h>
#include <stb_image.h>
#include <stb_image_write.h>

#include "jpge.h"

/*
camera -focalLength 12.7 -horizontalFilmAperture 1 -verticalFilmAperture 1 -name "cubeFront";
camera -focalLength 12.7 -horizontalFilmAperture 1 -verticalFilmAperture 1 -name "cubeRight"; rotate -y -90;
camera -focalLength 12.7 -horizontalFilmAperture 1 -verticalFilmAperture 1 -name "cubeBack"; rotate -y -180;
camera -focalLength 12.7 -horizontalFilmAperture 1 -verticalFilmAperture 1 -name "cubeLeft"; rotate -y 90;
camera -focalLength 12.7 -horizontalFilmAperture 1 -verticalFilmAperture 1 -name "cubeTop"; rotate -x 90;
camera -focalLength 12.7 -horizontalFilmAperture 1 -verticalFilmAperture 1 -name "cubeBottom"; rotate -x -90;
*/

int W = 2048;
int H = 1024;
volatile bool active;
std::atomic_int n_generated{ 0 };

namespace glm
{
    static float rgb2gl = 1.0f / 255.0f;
    static float gl2rgb = 255.0f;
    typedef glm::detail::tvec3<uint8_t, glm::highp> pix;
}

struct Range
{
    Range(float x0, float y0, float x1, float y1) : x0(x0), y0(y0), x1(x1), y1(y1) {}
    float x0, y0;
    float x1, y1;
};

typedef glm::vec2(*sampler)(float u, float v);
glm::vec2 sampler_front(float u, float v){ return glm::vec2(1 - (u * .5f + .5f), (v * .5f + .5f)); }
glm::vec2 sampler_back(float u, float v){ return glm::vec2(1 - (u * .5f + .5f), (v * .5f + .5f)); }
glm::vec2 sampler_right(float u, float v){ return glm::vec2((u * .5f + .5f), (v * .5f + .5f)); }
glm::vec2 sampler_left(float u, float v){ return glm::vec2(1 - (u * .5f + .5f), (v * .5f + .5f)); }
glm::vec2 sampler_top(float u, float v){ return glm::vec2(1 - (v * .5f + .5f), 1 - (u * .5f + .5f)); }
glm::vec2 sampler_bottom(float u, float v){ return glm::vec2((v * .5f + .5f), (u * .5f + .5f)); }
glm::vec2 sampler_stripe(float u, float v){ return glm::vec2((u * .5f + .5f), (v * .5f + .5f)); }

struct ImagePlane
{

    std::string name;
    std::vector<Range> ranges;
    glm::pix color;
    glm::vec3 plane;
    glm::vec3 normal;
    glm::mat3 rot;
    uint8_t* data;
    int width;
    int height;
    int comp;
    bool loaded;
    sampler sample_mapper;

    ImagePlane() : width(0), height(0), comp(0), loaded(false), data(nullptr) { }
    ImagePlane(std::string s, glm::pix c, glm::vec3 p, 
        std::initializer_list<Range> r, sampler map)
    {
        data = nullptr;
        sample_mapper = map;
        name = s;
        plane = glm::normalize(p);
        color = c;
        normal = -p;
        rot = glm::mat3(glm::orientation(normal, glm::vec3(0, 1, 0)));
        ranges = r;
    }

    glm::pix& operator()(int x, int y)
    {
        return *reinterpret_cast<glm::pix*>(data + (y * width + x) * comp);
    }

    bool load(std::string f)
    {
        unload();
        data = stbi_load(f.c_str(), &width, &height, &comp, 3);
        comp = 3; // TODO: handle transparent PNG's
        loaded = data != nullptr;
        return loaded;
    }

    void unload()
    {
        if (data)
            delete data;
        loaded = false;
    }

    glm::pix& sample(float u, float v)
    {
        // Normalize from [-1,1] to [0,1]
        glm::vec2 n = sample_mapper(u, v);

        // Convert to pixel coordinates
        int x = glm::clamp(static_cast<int>(n.x * width), 0, width);
        int y = glm::clamp(static_cast<int>(n.y * height), 0, height);

        return *reinterpret_cast<glm::pix*>(data + (y * width + x) * comp);
    }
};

struct ImageBuffer
{
    uint8_t* data;
    int width;
    int height;
    int comp;
    ImageBuffer() : width(0), height(height), comp(0), data(nullptr) { }
    ImageBuffer(int w, int h)
    {
        comp = 3;
        data = new uint8_t[w * h * comp];
        width = w;
        height = h;
    }
    glm::pix& operator()(int x, int y)
    {
        return *reinterpret_cast<glm::pix*>(data + (y * width + x) * comp);
    }
    glm::pix& operator()(int i)
    {
        return *reinterpret_cast<glm::pix*>(data + i * comp);
    }
    glm::vec3 gl(int i)
    {
        uint8_t* p = data + i * 3;
        return glm::vec3(p[0], p[1], p[2]) / 255.f;
    }
    ~ImageBuffer()
    {
        //if (data)
        //    delete data;
    }
};

struct Map
{
    ImagePlane *planes;
    int count;
    Map() : planes(nullptr), count(0) { }
    ~Map()
    {
        if (planes)
            delete[] planes;
        count = 0;
    }
};

struct CubeMap : public Map
{
    CubeMap()
    {
        planes = new ImagePlane[6];
        planes[0] = ImagePlane("cubeFront" , glm::pix(255, 0, 0), glm::vec3(-1, 0, 0), {Range(3./8., 1./4., 5./8., 3./4.)}, sampler_front);
        planes[1] = ImagePlane("cubeBack", glm::pix(0, 255, 0), glm::vec3(1, 0, 0), { Range(0. / 8., 1. / 4., 1. / 8., 3. / 4.), Range(7. / 8., 1. / 4., 8. / 8., 3. / 4.) }, sampler_back);
        planes[2] = ImagePlane("cubeRight", glm::pix(0, 0, 255), glm::vec3(0, -1, 0), { Range(5. / 8., 1. / 4., 7. / 8., 3. / 4.) }, sampler_right);
        planes[3] = ImagePlane("cubeLeft", glm::pix(255, 255, 0), glm::vec3(0, 1, 0), { Range(1. / 8., 1. / 4., 3. / 8., 3. / 4.) }, sampler_left);
        planes[4] = ImagePlane("cubeTop", glm::pix(0, 255, 255), glm::vec3(0, 0, -1), { Range(0. / 8., 5. / 8., 8. / 8., 8. / 8.) }, sampler_top);
        planes[5] = ImagePlane("cubeBottom", glm::pix(255, 0, 255), glm::vec3(0, 0, 1), { Range(0. / 8., 0. / 8., 8. / 8., 3. / 8.) }, sampler_bottom);
        planes[3].rot = glm::mat3();
    }
};

struct PolyMap : public Map
{
    PolyMap()
    {
        char str[128];
        count = 30;
        planes = new ImagePlane[count];
        for (int i = 0; i < count; i++)
        {
            float theta = (360.f / count) * i;
            float x = cosf(theta);
            float y = sinf(theta);
            //Range r{ (float)i / count, 0, (float)(i + 1) / count, 1 };
            Range r{ 0, 0, 1, 1 };
            glm::pix c{ (float)i / count * 255.f, 0, 0 };
            glm::vec3 p{ x, y, 0 };
            sprintf(str, "stripeTop%d", count - i);
            planes[i] = ImagePlane(str, c, p, { r }, sampler_stripe);
        }
    }
};

std::mutex generate_mutex;
std::deque<int> generate_queue;

int dequeue_generate()
{
    std::unique_lock<std::mutex> lock(generate_mutex);

    if (generate_queue.empty())
        return -1;

    int num = generate_queue.back();
    generate_queue.pop_back();

    return num;
}


struct ProjectionMap
{
    struct Projection
    {
        ImagePlane* plane;
        float u, v;
        Projection() : plane(nullptr), u(0), v(0) { }
        Projection(ImagePlane* plane, float u, float v) : plane(plane), u(u), v(v) { }
        glm::pix sample()
        {
            return plane->sample(u, v);
        }
    };
    ImageBuffer image;
    Projection* map_cache;
    std::vector<glm::ivec2> holes;
    ProjectionMap(int w, int h, uint8_t* data = nullptr)
    {
        if (data)
        {
            image.width = w;
            image.height = h;
            image.comp = 3;
            image.data = data;
        }
        else
        {
            image = ImageBuffer(w, h);
        }
        map_cache = nullptr;
    }
    void generateCache(Map& map)
    {
        map_cache = new Projection[image.width * image.height];
        concurrency::parallel_for(0, 6, [&](int i)
        //for (int i = 0; i < map.count; i++)
        {
            ImagePlane& plane = map.planes[i];
            glm::vec3 n = plane.normal;
            glm::mat3 rot = plane.rot;

            for (Range r : plane.ranges)
            {
                int x0 = static_cast<int>(r.x0 * W);
                int y0 = static_cast<int>(r.y0 * H);
                int x1 = static_cast<int>(r.x1 * W);
                int y1 = static_cast<int>(r.y1 * H);
                float w2 = W * 2.f;
                float h2 = H * 2.f;
                for (int y = y0; y < y1; y++)
                {
                    float v = (float)y / h2;
                    float theta = v * (float)M_PI_2;
                    float lz = cosf(theta);
                    float st = sinf(theta);
                    for (int x = x0; x < x1; x++)
                    {
                        float u = (float)x / (float)W * 2.f;
                        float v = (float)y / (float)H * 2.f;
                        float phi = u * (float)M_PI;         // phi
                        float theta = v * (float)M_PI * 0.5f; // theta

                        float lx = sinf(theta) * cosf(phi);
                        float ly = sinf(theta) * sinf(phi);
                        float lz = cosf(theta);

                        glm::vec3 l = glm::vec3(lx, ly, lz);

                        float t = -1.f / glm::dot(l, n);
                        if (t > 0)
                        {
                            glm::vec3 p = rot * (t * l);
                            if (fabs(p.x) < 0.212f && fabs(p.z) < 1.42f)
                            {
                                map_cache[y * image.width + x] = Projection(&plane, p.x / 0.212f, p.z / 1.42f);
                            }
                        }
                    }
                }
            }
        });
        int l = image.width * image.height;
        for (int y = 0; y < image.height; y++)
        {
            for (int x = 0; x < image.width; x++)
            {
                Projection& p = map_cache[y * image.width + x];
                if (!p.plane)
                    holes.emplace_back(x, H - y - 1);
            }
        }
    }
    void generate(Map& map)
    {
        if (!map_cache)
            generateCache(map);
        
        for (int y = 0; y < image.height; y++)
        {
            for (int x = 0; x < image.width; x++)
            {
                Projection& p = map_cache[y * image.width + x];
                if (p.plane)
                {
                    int bitmap_Y = H - y - 1;
                    image(x, bitmap_Y) = p.plane->sample(p.u, p.v);
                    //image(x, bitmap_Y) = p.plane->color;
                }
            }
        }

        for (glm::ivec2 i : holes)
        {
            glm::pix p0 = image(i.x - 1, i.y);
            glm::pix p1 = image(i.x + 1, i.y);
            uint8_t r = ((int)p0.r + (int)p1.r) / 2;
            uint8_t g = ((int)p0.g + (int)p1.g) / 2;
            uint8_t b = ((int)p0.b + (int)p1.b) / 2;
            glm::pix c(r, g, b);
            image(i.x, i.y) = c;
        }
    }
};

void generate_thread()
{
    char path[128];
    uint8_t* data = new uint8_t[W*H * 2 * 3];
    ProjectionMap proj(W, H, data);
    CubeMap cube;
    while (active)
    {
        int num = dequeue_generate();
        if (num == -1)
            return;
        
        proj.image.data = data;
        for (int i = 0; i < 6; i++)
        {
            sprintf(path, "Maya\\images\\vassel.0006\\%sL_%04d.jpg", cube.planes[i].name.c_str(), num);
            if (!cube.planes[i].load(path))
            {
                printf("Error opening %s\n", path);
                return;
            }
        }
        proj.generate(cube);

        proj.image.data = data + W*H * 3;
        for (int i = 0; i < 6; i++)
        {
            sprintf(path, "Maya\\images\\vassel.0006\\%sR_%04d.jpg", cube.planes[i].name.c_str(), num);
            if (!cube.planes[i].load(path))
            {
                printf("Error opening %s\n", path);
                return;
            }
        }
        proj.generate(cube);

        sprintf(path, "generated/vassel.0006_%04d.jpg", num);

        jpge::params params;
        params.m_quality = 70;
        bool saved = jpge::compress_image_to_jpeg_file(path, proj.image.width, 
            proj.image.height * 2, 3, data, params);
        if (!saved)
        {
            printf("Cannot write %s\n", path);
            exit(0);
        }
        n_generated++;
    }
}

int Test()
{
    char path[128];
    ProjectionMap proj(W, H);
    PolyMap map;

    for (int i = 0; i < map.count; i++)
    {
        sprintf(path, "Maya\\images\\circular\\%s.jpg", map.planes[i].name.c_str());
        if (!map.planes[i].load(path))
        {
            printf("Error opening %s\n", path);
            return -1;
        }
    }
    proj.generate(map);

    jpge::params params;
    params.m_quality = 70;
    sprintf(path, "generated/stripes30.jpg");
    jpge::compress_image_to_jpeg_file(path, proj.image.width,
        proj.image.height, 3, proj.image.data, params);

    return 0;
}

int _tmain(int argc, _TCHAR* argv[])
{
    return Test();

    std::vector<std::thread> gen_threads;
    active = true;

    int NT_GEN = 1;

    int start = 1;
    int end = 1;

    for (int i = start; i <= end; i++)
    {
        generate_queue.push_front(i);
    }
    
    for (int i = 0; i < NT_GEN; i++)
        gen_threads.emplace_back(generate_thread);

    int old_generated = 0;
    while (n_generated <= end - start)
    {
        int fps_gen = n_generated - old_generated;
        old_generated = n_generated;
        printf("Generated %04d (%2dfps)\r", n_generated, fps_gen);
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    printf("\nTerminated with %04d frames\n", n_generated);

    active = false;
    for (int i = 0; i < NT_GEN; i++)
        gen_threads[i].join();

    system("pause");

    return 0;
}


set PROJ=%~dp0.
set SCENE=vassel.0006.ma
start render -proj "%PROJ%" -cam cubeFrontL "%SCENE%"
start render -proj "%PROJ%" -cam cubeBackL "%SCENE%"
start render -proj "%PROJ%" -cam cubeRightL "%SCENE%"
start render -proj "%PROJ%" -cam cubeLeftL "%SCENE%"
start render -proj "%PROJ%" -cam cubeTopL "%SCENE%"
render -proj "%PROJ%" -cam cubeBottomL "%SCENE%"
start render -proj "%PROJ%" -cam cubeFrontR "%SCENE%"
start render -proj "%PROJ%" -cam cubeBackR "%SCENE%"
start render -proj "%PROJ%" -cam cubeRightR "%SCENE%"
start render -proj "%PROJ%" -cam cubeLeftR "%SCENE%"
start render -proj "%PROJ%" -cam cubeTopR "%SCENE%"
render -proj "%PROJ%" -cam cubeBottomR "%SCENE%"

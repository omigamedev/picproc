set PROJ=%~dp0.
set SCENE=test-scene.ma
start render -proj "%PROJ%" -cam cubeFrontR "%SCENE%"
start render -proj "%PROJ%" -cam cubeBackR "%SCENE%"
render -proj "%PROJ%" -cam cubeRightR "%SCENE%"
start render -proj "%PROJ%" -cam cubeLeftR "%SCENE%"
start render -proj "%PROJ%" -cam cubeTopR "%SCENE%"
start render -proj "%PROJ%" -cam cubeBottomR "%SCENE%"

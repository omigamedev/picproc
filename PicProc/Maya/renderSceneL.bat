set PROJ=%~dp0.
set SCENE=test-scene.ma
start render -proj "%PROJ%" -cam cubeFrontL "%SCENE%"
start render -proj "%PROJ%" -cam cubeBackL "%SCENE%"
render -proj "%PROJ%" -cam cubeRightL "%SCENE%"
start render -proj "%PROJ%" -cam cubeLeftL "%SCENE%"
start render -proj "%PROJ%" -cam cubeTopL "%SCENE%"
render -proj "%PROJ%" -cam cubeBottomL "%SCENE%"
